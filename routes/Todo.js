const express = require("express");
const router = express.Router();

//All functionalities
const {
  createTodo,
  getTodoById,
  getToDo,
  deleteTodo,
  getAllTodos,
  updateTodo,
} = require("../controllers/Todo");

//Parameters
//Fetch id
router.param("todoId", getTodoById);
//Get all todos
router.get("/todos/", getAllTodos);
//Idv todo
router.get("/todo/:todoId/",getToDo );
//Create todo
router.post("/todo/create/", createTodo);
//Update todo
router.put("/todo/:todoId/update", updateTodo);
//Delete todo
router.delete("/todo/:todoId/delete", deleteTodo);

module.exports = router;
