const Todo = require("../models/Todo");

//GET IDV TODO ID
exports.getTodoById = (req, res, next, todoId) => {
  Todo.findById(todoId).exec((err, todo) => {
    if (err || !todo) {
      return res.status(400).json({
        error: "404 todo not found",
      });
    }
    //store info so it can be used
    req.todo = todo;
    //since it's a middleqare we have to call next() so that it passes the control to th enext function in middleware
    next();
  });
};

//GET ALL TODO
exports.getAllTodos = (req, res) => {
  Todo.find()
    .sort("-createdAt")
    .exec((err, todos) => {
      if (err || !todos) {
        return res.status(400).json({
          error: "something went wrong in finding the todos",
        });
      }
      //return all todo
      res.json(todos);
    });
};

//GET IDV TODO
exports.getToDo = (req, res) => {
  return res.json(req.todo);
}

//CREATE A TODO
exports.createTodo = (req, res) => {
    const todo = new Todo(req.body)

    todo.save((err, task) => {
        if (err || !task) {
            return res.status(400).json({
              error: "something went wrong, a task could not be created",
            });
          }
          res.json({ task })
    })
}

//UPDATE A TODO
exports.updateTodo = (req, res) => {
    const todo = req.todo
    todo.task = req.body.task
    todo.save((err,t) =>{
        if(err || !t ){
        return res.status(400).json({ 
            error: "something went wrong"
        })
    }
    res.json(t)
    })
}

//DELETE A TODO
exports.deleteTodo = (req, res) => {
    const todo = req.todo
    todo.remove((err, task) => {
        if (err || !task){
            return res.status(400).json({
                error: "something went wrong while deleting the task"
            })
        }
        res.json({
            task_deleted: task,
            message: "task deleted successfully",
        })
    })
}